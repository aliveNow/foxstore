//
//  AFAppDelegate.h
//  FoxStore
//
//  Created by Маша on 10.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
