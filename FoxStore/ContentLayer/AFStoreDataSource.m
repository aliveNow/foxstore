//
//  AFStoreDataSource.m
//  FoxStore
//
//  Created by Маша on 11.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import "AFStoreDataSource.h"
#import "AFDatabaseManager.h"
#import "AFObjectConverter.h"

@interface AFStoreDataSource ()
@property (strong, nonatomic) AFDatabaseManager *database;
@property (strong, nonatomic) id<AFObjectConverter> converter;
@end

@implementation AFStoreDataSource

- (id)init
{
    self = [super init];
    if (self) {
        self.database = [[AFDatabaseManager alloc] init];
        self.converter = [AFDatabaseManager getDefaultConverter];
    }
    return self;
}

- (NSArray *)getAllProducts
{
    return [self.converter convertToUpperLayerObjects:[self.database getAllProducts]];
}

#pragma mark - Изменения корзины

+ (AFBasket *) getCurrentBasket
{
    static AFBasket *currentBasket = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        AFStoreDataSource *dataSource = [self defaultInstance];
        id localBasket = [dataSource.database getCurrentOrder];
        if (!localBasket) {
            localBasket = [dataSource.database getNewOrder];
        }
        currentBasket = [dataSource.converter convertToUpperLayerObject:localBasket];
        NSNotificationCenter *nCenter = [NSNotificationCenter defaultCenter];
        [nCenter addObserver:dataSource selector:@selector(basketDidChangeItemInSomeWay:) name:AFBasketDidAddItemNotification object:currentBasket];
        [nCenter addObserver:dataSource selector:@selector(basketDidChangeItemInSomeWay:) name:AFBasketDidRemoveItemNotification object:currentBasket];
        [nCenter addObserver:dataSource selector:@selector(basketDidChangeItemInSomeWay:) name:AFBasketDidChangeItemNotification object:currentBasket];
#warning и где мне отписываться от этой красоты?
    });
    return currentBasket;
}

+ (instancetype) defaultInstance
{
    static AFStoreDataSource *defaultInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        defaultInstance = [[self alloc] init];
    });
    return defaultInstance;
}

- (id) localItemByObject:(AFBasketItem *)item fromBasket:(AFBasket *)basket
{
    return [self.database orderItemByProduct:[self.converter convertToLowerLayerObject:item.product]
                                    forOrder:[self.converter convertToLowerLayerObject:basket]];
}

- (void) basketDidChangeItemInSomeWay:(NSNotification *)notification
{
    AFBasket *basket = notification.object;
    AFBasketItem *basketItem = notification.userInfo[AFBasketItemInfoKey];
    id localItem = [self localItemByObject:basketItem fromBasket:basket];
    if ([notification.name isEqualToString:AFBasketDidRemoveItemNotification]) {
        [self.database removeObject:localItem];
    }else {
        [self.converter transferChangesFromObject:basketItem toObject:localItem];
    }
    [self.database saveChanges];
}

@end
