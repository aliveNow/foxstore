//
//  AFProduct.h
//  FoxStore
//
//  Created by Маша on 11.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AFProduct : NSObject
#warning пересекается с NSData - переименовать что-ли?
@property (strong, nonatomic, readonly) id data;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *fullDescription;
@property (strong, nonatomic) NSString *imagePath;
@property (assign, nonatomic) double price;
@property (assign, nonatomic) NSUInteger amount;

- (instancetype) initWithData:(id)data;

- (void) setPrimaryData:(id)data; // можно установить только если data - nil

@end
