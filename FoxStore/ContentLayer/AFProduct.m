//
//  AFProduct.m
//  FoxStore
//
//  Created by Маша on 11.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import "AFProduct.h"

@interface AFProduct ()
@property (strong, nonatomic) id data;
@end

@implementation AFProduct

- (instancetype)initWithData:(id)data
{
    self = [self init];
    if (self) {
        self.data = data;
    }
    return self;
}

- (void) setPrimaryData:(id)data
{
    if (data == nil) {
        _data = data;
    }
}

- (BOOL)isEqual:(id)object
{
    return [self class] == [object class] && [self.data isEqual:[object data]];
}

- (NSString *)imagePath
{
    return (_imagePath) ? _imagePath : @"emptyProduct.png";
}

@end
