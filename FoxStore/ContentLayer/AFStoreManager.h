//
//  AFStoreManager.h
//  FoxStore
//
//  Created by Маша on 13.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFBasket.h"

// названия уведомлений
extern NSString *const AFStoreWillAddItemNotification;
extern NSString *const AFStoreDidAddItemNotification;
extern NSString *const AFStoreDidChangeItemNotification;

// ключи уведомлений
extern NSString *const AFStoreItemInfoKey;

typedef void (^AFSaveCompletionHandler)(BOOL success, NSError *error);

@interface AFStoreManager : NSObject

+ (instancetype) defaultInstance;

- (void) saveProduct:(AFProduct *)product withCompletion:(AFSaveCompletionHandler)completionBlock;
- (void) sendOrder:(AFBasket *)basket withCompletion:(AFSaveCompletionHandler)completionBlock;

// заново запускает операции сохранения новых продуктов
- (void) recoverTerminatedOperations;
@end
