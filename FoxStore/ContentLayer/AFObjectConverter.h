//
//  AFObjectConverter.h
//  FoxStore
//
//  Created by Маша on 12.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AFObjectConverter <NSObject>

- (NSArray *) convertToUpperLayerObjects:(NSArray *)objects;
- (NSArray *) convertToLowerLayerObjects:(NSArray *)objects;

- (id) convertToUpperLayerObject:(id)object;
- (id) convertToLowerLayerObject:(id)object;

- (void) transferChangesFromObject:(id)fromObject toObject:(id)toObject;

@end
