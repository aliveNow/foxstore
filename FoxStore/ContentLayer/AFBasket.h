//
//  AFBasket.h
//  FoxStore
//
//  Created by Маша on 11.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFProduct.h"

// названия уведомлений
extern NSString *const AFBasketDidRemoveItemNotification;
extern NSString *const AFBasketDidAddItemNotification;
extern NSString *const AFBasketDidChangeItemNotification;

// ключи уведомлений
extern NSString *const AFBasketItemInfoKey;
extern NSString *const AFBasketItemIndexInfoKey;

@interface AFBasketItem : NSObject
@property (strong, nonatomic, readonly) AFProduct *product;
@property (assign, nonatomic, readonly) NSUInteger amount;
@end

@interface AFBasket : NSObject
@property (strong, nonatomic, readonly) id data;

- (instancetype)initWithData:(id)data;
- (void) beginNewOrderWithData:(id)data;

- (NSArray *) allProducts;
- (NSArray *) allItems;

- (BOOL) addProduct:(AFProduct *)product withAmount:(NSUInteger)amount;
- (void) removeProduct:(AFProduct *)product;
- (void) removeProductAtIndex:(NSUInteger)index;

- (NSUInteger) amountOfOrderedProduct:(AFProduct *)product;
- (double) totalPrice;

- (NSUInteger)countOfAllItems;
- (id)objectAtIndexedSubscript:(NSUInteger)idx; // для удобства обращения к коллекции items
- (id)objectForKeyedSubscript:(id)key; // возвращает AFBasketItem по продукту

@end
