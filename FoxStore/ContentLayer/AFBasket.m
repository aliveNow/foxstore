//
//  AFBasket.m
//  FoxStore
//
//  Created by Маша on 11.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import "AFBasket.h"

NSString *const AFBasketDidRemoveItemNotification = @"AFBasketDidRemoveItemNotification";
NSString *const AFBasketDidAddItemNotification = @"AFBasketDidAddItemNotification";
NSString *const AFBasketDidChangeItemNotification = @"AFBasketDidChangeItemNotification";
NSString *const AFBasketItemInfoKey = @"item";
NSString *const AFBasketItemIndexInfoKey = @"index";

@interface AFBasketItem ()
@property (strong, nonatomic) AFProduct *product;
@property (assign, nonatomic) NSUInteger amount;
@end

@implementation AFBasketItem
@end

@interface AFBasket ()
@property (strong, nonatomic) id data;
@property (strong, nonatomic) NSMutableArray *items;
@end

@implementation AFBasket

- (id)init
{
    self = [super init];
    if (self) {
        self.items = @[].mutableCopy;
    }
    return self;
}

- (instancetype)initWithData:(id)data
{
    self = [self init];
    if (self) {
        self.data = data;
    }
    return self;
}

- (void) beginNewOrderWithData:(id)data
{
    [self clear];
    self.data = data;
}

- (NSArray *)allProducts
{
    return [self.items bk_map:^(AFBasketItem *item) {
        return item.product;
    }];
}

- (NSArray *)allItems
{
    return [self.items copy];
}

- (double)totalPrice
{
    double result = 0.0;
    for (AFBasketItem *item in self.items) {
        result += item.product.price * item.amount;
    }
    return result;
}

#pragma mark - Методы коллекции

- (id)objectAtIndexedSubscript:(NSUInteger)idx
{
    return self.items[idx];
}

- (id)objectForKeyedSubscript:(id)key
{
    return [self itemByProduct:key];
}

- (NSUInteger)countOfAllItems
{
    return self.items.count;
}

- (AFBasketItem *) itemByProduct:(AFProduct *)product
{
    return [self.items bk_match:^(AFBasketItem *item) {
        return [item.product isEqual:product];
    }];
}

- (NSUInteger) indexOfItemByProduct:(AFProduct *)product
{
    __block NSUInteger index = NSNotFound;
    [self.items enumerateObjectsUsingBlock:^(AFBasketItem *item, NSUInteger idx, BOOL *stop) {
        if ([item.product isEqual:product]) {
            index = idx;
            *stop = YES;
        }
    }];
    return index;
}

- (NSUInteger) amountOfOrderedProduct:(AFProduct *)product
{
    return [self[product] amount];
}

#pragma mark - Добавление/удаление элементов

- (BOOL) addProduct:(AFProduct *)product withAmount:(NSUInteger)amount
{
    if (product.amount >= amount) {
        if (amount == 0) {
            [self removeProduct:product];
        }else {
            NSUInteger index = [self indexOfItemByProduct:product];
            AFBasketItem *item = nil;
            BOOL isNewItem = (index == NSNotFound);
            if (isNewItem) {
                item = [[AFBasketItem alloc] init];
                item.product = product;
                [self.items addObject:item];
            }else {
                item = self.items[index];
            }
            item.amount = amount;
            if (isNewItem) {
                [[NSNotificationCenter defaultCenter] postNotificationName:AFBasketDidAddItemNotification
                                                                    object:self
                                                                  userInfo:@{AFBasketItemInfoKey:item,
                                                                             AFBasketItemIndexInfoKey:@(self.items.count - 1)                                                                             }];
            }else {
                [[NSNotificationCenter defaultCenter] postNotificationName:AFBasketDidChangeItemNotification
                                                                    object:self
                                                                  userInfo:@{AFBasketItemInfoKey:item,
                                                                             AFBasketItemIndexInfoKey:@(index)                                                                             }];
            }
        }
        return YES;
    }
    return NO;
}

- (void) removeProductAtIndex:(NSUInteger)index
{
    if (index != NSNotFound && index < self.items.count) {
        AFBasketItem *item = self.items[index];
        [self.items removeObjectAtIndex:index];
        [[NSNotificationCenter defaultCenter] postNotificationName:AFBasketDidRemoveItemNotification
                                                            object:self
                                                          userInfo:@{AFBasketItemInfoKey:item,
                                                                     AFBasketItemIndexInfoKey:@(index)
                                                                     }];
    }
}

- (void)removeProduct:(AFProduct *)product
{
    NSUInteger index = [self indexOfItemByProduct:product];
    [self removeProductAtIndex:index];
}

- (void) clear
{
    [self.items removeAllObjects];
}

@end
