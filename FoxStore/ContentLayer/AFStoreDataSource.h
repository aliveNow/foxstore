//
//  AFStoreDataSource.h
//  FoxStore
//
//  Created by Маша on 11.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFBasket.h"

@interface AFStoreDataSource : NSObject
- (NSArray *) getAllProducts;

+ (AFBasket *) getCurrentBasket;

@end
