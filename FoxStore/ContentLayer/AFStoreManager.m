//
//  AFStoreManager.m
//  FoxStore
//
//  Created by Маша on 13.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import "AFStoreManager.h"
#import "AFDatabaseManager.h"

NSString *const AFStoreWillAddItemNotification = @"AFStoreWillAddItemNotification";
NSString *const AFStoreDidAddItemNotification = @"AFStoreDidAddItemNotification";
NSString *const AFStoreDidChangeItemNotification = @"AFStoreDidChangeItemNotification";

NSString *const AFStoreItemInfoKey = @"AFStoreItemInfoKey";

@interface AFStoreManager ()
@property (strong, nonatomic) AFDatabaseManager *database;
@property (strong, nonatomic) id<AFObjectConverter> converter;
@property (strong, nonatomic) NSOperationQueue *operationQueue;
@end

@implementation AFStoreManager

- (id)init
{
    self = [super init];
    if (self) {
        self.database = [[AFDatabaseManager alloc] init];
        self.converter = [AFDatabaseManager getDefaultConverter];
        self.operationQueue = [[NSOperationQueue alloc] init];
    }
    return self;
}

+ (instancetype) defaultInstance
{
    static AFStoreManager *defaultInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        defaultInstance = [[self alloc] init];
    });
    return defaultInstance;
}

#pragma mark - обработка сохранения изменений

- (void) saveProduct:(AFProduct *)product withCompletion:(AFSaveCompletionHandler)completionBlock
{
    if (product.data) {
        [self inner_updateProduct:product withCompletion:completionBlock];
    }else {
        [self inner_saveProduct:product withCompletion:completionBlock];
    }
}

- (void) inner_saveProduct:(AFProduct *)product withCompletion:(AFSaveCompletionHandler)completionBlock
{
    id localProduct = [self.converter convertToLowerLayerObject:product];
    [self.database saveObject:localProduct];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:AFStoreWillAddItemNotification object:self userInfo:@{AFStoreItemInfoKey:product}];
    
    // изображаем длительную операцию
    __weak typeof(self) weakSelf = self;
    [self.operationQueue addOperationWithBlock:^() {
        sleep(3);
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf) {
            // проставляем на объекте галочку, что он пригоден к показу
            [strongSelf.database approveProduct:localProduct];
            [strongSelf.database saveObject:localProduct];
            
            // отсылаем все положенные оповещения
            [[NSNotificationCenter defaultCenter] postNotificationName:AFStoreDidAddItemNotification object:strongSelf userInfo:@{AFStoreItemInfoKey:product}];
            
            // вызываем блок завершения
            if (completionBlock) {
                completionBlock(YES, nil);
            }
        }else {
            if (completionBlock) {
                completionBlock(NO, [NSError errorWithDomain:@"Something went terribly wrong" code:0 userInfo:nil]);
            }
        }
    }];
}

- (void) inner_updateProduct:(AFProduct *)product withCompletion:(AFSaveCompletionHandler)completionBlock
{
    id localProduct = [self.converter convertToLowerLayerObject:product];
    [self.converter transferChangesFromObject:product toObject:localProduct];
    [self.database saveObject:localProduct];
    [[NSNotificationCenter defaultCenter] postNotificationName:AFStoreDidChangeItemNotification object:self userInfo:@{AFStoreItemInfoKey:product}];
    if (completionBlock) {
        completionBlock(YES, nil);
    }
}

- (void) sendOrder:(AFBasket *)basket withCompletion:(AFSaveCompletionHandler)completionBlock
{
    // забираем заказ из корзины
    id order = [self.converter convertToLowerLayerObject:basket];
    __weak typeof(self) weakSelf = self;
    [self.operationQueue addOperationWithBlock:^() {
        sleep(3);
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf) {
            // проставляем у заказа новый статус
            [strongSelf.database sendOrder:order];
            // создаем новый заказ в базе, передаем его в корзину
            dispatch_sync(dispatch_get_main_queue(), ^() {
                id newOrder = [strongSelf.database getNewOrder];
                [basket beginNewOrderWithData:newOrder];
            });
#warning изобразить ошибку что-ли?
            if (completionBlock) {
                completionBlock(YES, nil);
            }
        }else {
            if (completionBlock) {
                completionBlock(NO, [NSError errorWithDomain:@"Something went terribly wrong" code:0 userInfo:nil]);
            }
        }
    }];
}

#pragma mark - Возобновление операций после срыва. Или иначе "финт ушами во имя задания, которое я еще возможно и неправильно понимаю"

- (void) recoverTerminatedOperations
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSArray *newProducts = [self.database getNewProducts];
        if (newProducts.count > 0) {
            NSArray *products = [self.converter convertToUpperLayerObjects:newProducts];
            [products bk_each:^(AFProduct *obj) {
                [self inner_saveProduct:obj withCompletion:NULL]; // кому нужно узнать - может подключиться к notification
            }];
        }
    });
}

@end
