//
//  AFFlipContainerSegue.m
//  FoxStore
//
//  Created by Маша on 10.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import "AFFlipContainerSegue.h"

@implementation AFFlipContainerSegue

- (void)perform
{
    UIViewController *fromController = self.sourceViewController;
    if ([fromController.parentViewController isKindOfClass:[UINavigationController class]] || [fromController.parentViewController isKindOfClass:[UITabBarController class]]) {
        fromController = fromController.parentViewController;
    }
    [self flipFromViewController:fromController toViewController:self.destinationViewController withDirection:UIViewAnimationOptionTransitionFlipFromLeft];
}

- (void) flipFromViewController:(UIViewController *)fromController
               toViewController:(UIViewController *)toController
                  withDirection:(UIViewAnimationOptions)direction
{
    UIViewController *parentVC = [fromController parentViewController];
    
    toController.view.frame = fromController.view.bounds;
    [parentVC addChildViewController:toController];
    [fromController willMoveToParentViewController:nil];
    
    [parentVC transitionFromViewController:fromController
                          toViewController:toController
                                  duration:0.5
                                   options:direction | UIViewAnimationOptionCurveEaseIn
                                animations:nil
                                completion:^(BOOL finished) {
                                    [toController didMoveToParentViewController:parentVC];
                                    [fromController removeFromParentViewController];
                                }];
}

@end
