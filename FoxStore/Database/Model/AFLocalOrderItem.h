//
//  AFLocalOrderItem.h
//  FoxStore
//
//  Created by Маша on 11.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AFLocalOrder;
@class AFLocalProduct;

@interface AFLocalOrderItem : NSManagedObject

@property (nonatomic, retain) NSNumber * amount;
@property (nonatomic, retain) AFLocalProduct *product;
@property (nonatomic, retain) AFLocalOrder *order;

@end
