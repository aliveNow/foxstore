//
//  AFLocalOrder.m
//  FoxStore
//
//  Created by Маша on 11.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import "AFLocalOrder.h"
#import "AFLocalOrderItem.h"


@implementation AFLocalOrder

@dynamic createdAt;
@dynamic status;
@dynamic items;

@end
