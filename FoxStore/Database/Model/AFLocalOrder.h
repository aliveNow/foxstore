//
//  AFLocalOrder.h
//  FoxStore
//
//  Created by Маша on 11.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AFLocalOrderItem;

@interface AFLocalOrder : NSManagedObject

@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSNumber * status;
@property (nonatomic, retain) NSSet *items;
@end

@interface AFLocalOrder (CoreDataGeneratedAccessors)

- (void)addItemsObject:(AFLocalOrderItem *)value;
- (void)removeItemsObject:(AFLocalOrderItem *)value;
- (void)addItems:(NSSet *)values;
- (void)removeItems:(NSSet *)values;

@end
