//
//  AFLocalProduct.h
//  FoxStore
//
//  Created by Маша on 11.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class AFLocalOrderItem;

@interface AFLocalProduct : NSManagedObject

@property (nonatomic, retain) NSNumber * amount;
@property (nonatomic, retain) NSString * fullDescription;
@property (nonatomic, retain) NSString * imagePath;
@property (nonatomic, retain) NSNumber * isNew;
@property (nonatomic, retain) NSNumber * price;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSSet *orderItems;
@end

@interface AFLocalProduct (CoreDataGeneratedAccessors)

- (void)addOrderItemsObject:(AFLocalOrderItem *)value;
- (void)removeOrderItemsObject:(AFLocalOrderItem *)value;
- (void)addOrderItems:(NSSet *)values;
- (void)removeOrderItems:(NSSet *)values;

@end
