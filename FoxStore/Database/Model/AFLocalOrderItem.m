//
//  AFLocalOrderItem.m
//  FoxStore
//
//  Created by Маша on 11.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import "AFLocalOrderItem.h"


@implementation AFLocalOrderItem

@dynamic amount;
@dynamic product;
@dynamic order;

@end
