//
//  AFDatabaseManager.m
//  FoxStore
//
//  Created by Маша on 12.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import "AFDatabaseManager.h"
#import "AFDBToContentConverter.h"
#import <NSManagedObject+MagicalDataImport.h>
#import <NSManagedObject+MagicalFinders.h>
#import <NSManagedObject+MagicalRequests.h>
#import <NSManagedObject+MagicalRecord.h>
#import <NSManagedObjectContext+MagicalSaves.h>
#import <NSManagedObjectContext+MagicalThreading.h>
#import "AFLocalProduct.h"
#import "AFLocalOrder.h"
#import "AFLocalOrderItem.h"

@interface NSManagedObject (AFUtils)
+ (id) af_findFirstWithPredicate:(NSPredicate *)predicate;
@end

@implementation NSManagedObject (AFUtils)

+ (id) af_findFirstWithPredicate:(NSPredicate *)predicate
{
    NSFetchRequest *request = [self MR_requestFirstWithPredicate:predicate];
    request.returnsObjectsAsFaults = NO;
    return [[self MR_executeFetchRequest:request] lastObject];
}

@end

@implementation AFDatabaseManager

- (NSArray *) getAllProducts
{
    return [[self class] getProductsByPredicate:[NSPredicate predicateWithFormat:@"isNew = NO"]];
}

- (NSArray *) getNewProducts
{
    return [[self class] getProductsByPredicate:[NSPredicate predicateWithFormat:@"isNew = YES"]];
}

+ (NSArray *) getProductsByPredicate:(NSPredicate *)predicate
{
    NSFetchRequest *request = [AFLocalProduct MR_requestAllSortedBy:@"createdAt" ascending:NO withPredicate:predicate];
    request.returnsObjectsAsFaults = NO;
    return [NSManagedObject MR_executeFetchRequest:request];
}

#pragma mark - Заказ

- (id) findFirstEntityObject:(Class)entityClass withPredicate:(NSPredicate *)predicate
{
    NSFetchRequest *request = [entityClass MR_requestFirstWithPredicate:predicate];
    request.returnsObjectsAsFaults = NO;
    return [[NSManagedObject MR_executeFetchRequest:request] lastObject];
}

- (id) orderItemByProduct:(id)product forOrder:(id)order
{
    AFLocalOrderItem *item = [AFLocalOrderItem af_findFirstWithPredicate:[NSPredicate predicateWithFormat:@"order = %@ && product = %@", order, product]];
    
    if (!item) {
        item = [AFLocalOrderItem MR_createEntity];
        item.order = order;
        item.product = product;
    }
    return item;
}

- (id) getCurrentOrder
{
    NSFetchRequest *request = [AFLocalOrder MR_requestFirstWithPredicate:[NSPredicate predicateWithFormat:@"status = 0"]];
    request.returnsObjectsAsFaults = NO;
    // Сразу вытащить все продукты на items, чтобы не было повторных запросов за faults
    request.relationshipKeyPathsForPrefetching = @[@"items", @"items.product"];
    AFLocalOrder *order = [[AFLocalOrder MR_executeFetchRequest:request] lastObject];
    return order;
}

- (id) getNewOrder
{
    AFLocalOrder *order = [AFLocalOrder MR_createEntity];
    order.createdAt = [NSDate date];
    return order;
}

- (void) sendOrder:(id)order
{
    // так как это простейшая реализация - статусов назначаю два: 0 и 1
    AFLocalOrder *localOrder = order;
    localOrder.createdAt = [NSDate date];
    localOrder.status = @1;
    // уменьшить количество продуктов на заказанное число
    for (AFLocalOrderItem *item in localOrder.items) {
        item.product.amount = @([item.product.amount integerValue] - [item.amount integerValue]);
    }
    [localOrder.managedObjectContext MR_saveToPersistentStoreAndWait];
}

#pragma mark - Utils

- (void) approveProduct:(id)object
{
    NSAssert([object isKindOfClass:[AFLocalProduct class]], @"Not an object that was expected. Need AFLocalProduct there.");
    [object setValue:@NO forKey:@"isNew"];
}

- (void) saveObject:(id)object
{
    [[object managedObjectContext ] MR_saveToPersistentStoreAndWait];
}

- (void) removeObject:(id)object
{
    [object MR_deleteEntity];
}

- (void) saveChanges
{
    [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
}

+ (id<AFObjectConverter>) getDefaultConverter
{
    return [[AFDBToContentConverter alloc] init];
}

#pragma mark - первоначальный контент базы

+ (void) createDatabase
{
    static NSString *AFDatabaseManagerDatabaseWasCreatedKey = @"AFDatabaseManagerDatabaseWasCreatedKey";
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL wasCreated = [ud boolForKey:AFDatabaseManagerDatabaseWasCreatedKey];
    if (!wasCreated) {
        
        NSArray *data = @[
                          @{@"imagePath": @"product1", @"title":@"Клавиатура неубиваемая", @"price": @1200.10, @"amount": @20},
                          @{@"imagePath": @"product2", @"title":@"Шахматы", @"price": @400.00, @"amount": @42},
                          @{@"imagePath": @"product3", @"title":@"Тарелка спутниковая накрышная", @"price": @4300, @"amount": @6},
                          @{@"imagePath": @"product4", @"title":@"Печенье в ассортименте", @"price": @50.4, @"amount": @150},
                          @{@"imagePath": @"product5", @"title":@"Морепродукты", @"price": @300, @"amount": @70},
                          @{@"imagePath": @"product6", @"title":@"Лампа, освещающая путь", @"price": @700, @"amount": @15},
                          ];
        for (NSDictionary *dict in data) {
            AFLocalProduct *product = [AFLocalProduct MR_createEntity];
            [product MR_importValuesForKeysWithObject:dict];
            product.createdAt = [NSDate date];
        }
        [[NSManagedObjectContext MR_contextForCurrentThread] MR_saveToPersistentStoreAndWait];
        [ud setBool:YES forKey:AFDatabaseManagerDatabaseWasCreatedKey];
        [ud synchronize];
    }
}

@end
