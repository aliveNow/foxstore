//
//  AFDBToContentConverter.h
//  FoxStore
//
//  Created by Маша on 12.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFObjectConverter.h"

// конвертирует объекты из NSManagedObject в объекты интерфейса и обратно
@interface AFDBToContentConverter : NSObject <AFObjectConverter>

@end
