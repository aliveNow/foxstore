//
//  AFDatabaseManager.h
//  FoxStore
//
//  Created by Маша on 12.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFObjectConverter.h"

@interface AFDatabaseManager : NSObject

- (NSArray *) getAllProducts;
- (NSArray *) getNewProducts; // продукты, которые еще не были "одобрены", поэтому не показываются пользователю

- (void) approveProduct:(id)object;

- (id) getCurrentOrder;
- (id) getNewOrder;

- (void) sendOrder:(id)order;

- (id) orderItemByProduct:(id)product forOrder:(id)order;

- (void) saveObject:(id)object;
- (void) removeObject:(id)object;
- (void) saveChanges;

// конвертирует объекты из NSManagedObject в объекты интерфейса и обратно
+ (id<AFObjectConverter>) getDefaultConverter;

+ (void) createDatabase;

@end
