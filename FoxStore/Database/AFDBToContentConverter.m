//
//  AFDBToContentConverter.m
//  FoxStore
//
//  Created by Маша on 12.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import "AFDBToContentConverter.h"
#import "AFBasket.h"
#import "AFLocalProduct.h"
#import "AFLocalOrder.h"
#import "AFLocalOrderItem.h"
#import <NSManagedObject+MagicalFinders.h>
#import <NSManagedObject+MagicalRecord.h>

@implementation AFDBToContentConverter

- (NSArray *) convertToUpperLayerObjects:(NSArray *)objects
{
    NSManagedObject *litmusPaper = [objects lastObject];
    NSString *entityName = [[litmusPaper entity] name];
    NSArray *result = nil;
#warning нужно будет перейти на константы
    if ([entityName isEqualToString:@"AFLocalProduct"]) {
        result = [objects bk_map:^(AFLocalProduct *obj) {
            AFProduct *product = [[AFProduct alloc] initWithData:obj];
            [self transferChangesFromObject:obj toObject:product];
            return product;
        }];
    }else if ([entityName isEqualToString:@"AFLocalOrder"]) {
        // заказу соответствует корзина в объектах контента
        result = [objects bk_map:^(AFLocalOrder *obj) {
            AFBasket *basket = [[AFBasket alloc] initWithData:obj];
            // Добавить в корзину все сохраненные продукты
            NSArray *lowerItems = [obj.items allObjects];
            [lowerItems bk_each:^(AFLocalOrderItem *item) {
                [basket addProduct:[self convertToUpperLayerObject:item.product]
                        withAmount:[item.amount integerValue]];
            }];
            return basket;
        }];
    }
    return result;
}

- (NSArray *) convertToLowerLayerObjects:(NSArray *)objects
{
    NSManagedObject *litmusPaper = [objects lastObject];
    NSArray *result = nil;
    if ([litmusPaper respondsToSelector:@selector(data)]) {
        result = [objects bk_map:^(id obj) {
            if ([obj data]) {
                return (id)[obj data];
            }
            id newData = [self createDatabaseObjectForContentObject:obj];
            if ([obj isKindOfClass:[AFProduct class]]) {
                [obj setPrimaryData:newData];
            }
            return newData;
        }];
    }
    return result;
}

- (id) convertToUpperLayerObject:(id)object
{
    return object? [[self convertToUpperLayerObjects:@[object]] lastObject] : nil;
}

- (id) convertToLowerLayerObject:(id)object
{
    return object? [[self convertToLowerLayerObjects:@[object]] lastObject] : nil;
}

#pragma mark - создание объекта БД

- (NSString *) entityNameByContentObject:(id)contentObject
{
    if ([contentObject isKindOfClass:[AFProduct class]]) {
        return @"AFLocalProduct";
    }else if ([contentObject isKindOfClass:[AFBasket class]]) {
        return @"AFLocalOrder";
    }
    return nil;
}

- (id) createDatabaseObjectForContentObject:(id)contentObject
{
    NSString *entityName = [self entityNameByContentObject:contentObject];
    Class entityClass = NSClassFromString(entityName);
    NSManagedObject *newObj = [entityClass MR_createEntity];
    if (entityClass == [AFLocalProduct class]) {
        AFLocalProduct *o2 = (id)newObj;
        // Чтобы продукт не выводился в списке сразу - для видимости "асинхронных" операций
        o2.isNew = @YES;
        o2.createdAt = [NSDate date];
    }
    [self transferChangesFromObject:contentObject toObject:newObj];
    return newObj;
}

#pragma mark - перенос изменений из объекта в объект

// Единственное безобразие, из-за которого всю эту историю лучше делать попроще
- (void) transferChangesFromObject:(id)fromObject toObject:(id)toObject
{
    if ([fromObject isKindOfClass:[NSManagedObject class]]) {
        NSString *entityName = [[fromObject entity] name];
        if ([entityName isEqualToString:@"AFLocalProduct"]) {
            AFLocalProduct *o1 = fromObject;
            AFProduct *o2 = toObject;
            o2.title = o1.title;
            o2.fullDescription = o1.fullDescription;
            o2.imagePath = o1.imagePath;
            o2.amount = [o1.amount integerValue];
            o2.price = [o1.price doubleValue];
        }else if ([entityName isEqualToString:@"AFLocalOrder"]) {
            // ничего пока
        }else if ([entityName isEqualToString:@"AFLocalOrderItem"]) {
            // невозможно
        }
    }else {
        NSString *entityName = [[toObject entity] name];
        if ([entityName isEqualToString:@"AFLocalProduct"]) {
            AFProduct *o1 = fromObject;
            AFLocalProduct *o2 = toObject;
            o2.title = o1.title;
            o2.fullDescription = o1.fullDescription;
            o2.imagePath = o1.imagePath;
            o2.amount = @(o1.amount);
            o2.price = @(o1.price);
        }else if ([entityName isEqualToString:@"AFLocalOrder"]) {
            // ничего пока
        }else if ([entityName isEqualToString:@"AFLocalOrderItem"]) {
            AFBasketItem *o1 = fromObject;
            AFLocalOrderItem *o2 = toObject;
            //o2.product = o1.product.data;
            o2.amount = @(o1.amount);
        }
    }
}

@end
