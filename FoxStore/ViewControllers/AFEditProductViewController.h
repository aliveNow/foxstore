//
//  AFEditProductViewController.h
//  FoxStore
//
//  Created by Маша on 13.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFProductPresenterProtocol.h"

@interface AFEditProductViewController : UIViewController <AFProductPresenterProtocol>
@property (strong, nonatomic) AFProduct *product;
@end
