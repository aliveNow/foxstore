//
//  AFSellerListOfProductsVC.m
//  FoxStore
//
//  Created by Маша on 13.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import "AFSellerListOfProductsVC.h"
#import "AFProductPresenterProtocol.h"
#import "AFBuyerProductCell.h"
#import "AFStoreDataSource.h"
#import "AFStoreManager.h"

#import <SVProgressHUD.h>

@interface AFSellerListOfProductsVC ()
@property (strong, nonatomic) AFStoreDataSource *dataSource;
@property (strong, nonatomic) NSArray *products;

@property (strong, nonatomic) NSIndexPath *selectedIndexPath;
@property (assign, nonatomic) NSUInteger numberOfFutureProducts;
@end

@implementation AFSellerListOfProductsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.dataSource = [[AFStoreDataSource alloc] init];
    self.products = [self.dataSource getAllProducts];
    
    AFStoreManager *manager = [AFStoreManager defaultInstance];
    NSNotificationCenter *nCenter = [NSNotificationCenter defaultCenter];
    // подписываемся только на уведомление добавления, update-запросы проходят сразу - и это фича
    [nCenter addObserver:self selector:@selector(storeManagerWillAddProduct:) name:AFStoreWillAddItemNotification object:manager];
    [nCenter addObserver:self selector:@selector(storeManagerDidAddProduct:) name:AFStoreDidAddItemNotification object:manager];
    // возобновляем все операции, которые возможно, были прерваны
    [manager recoverTerminatedOperations];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.products.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *productCellId = @"AFBuyerProductCellId";
    AFBuyerProductCell *cell = [tableView dequeueReusableCellWithIdentifier:productCellId forIndexPath:indexPath];
    AFProduct *product = self.products[indexPath.row];
    cell.image.image = [UIImage imageNamed:product.imagePath];
    cell.titleLabel.text = product.title;
    cell.priceLabel.text = [NSString stringWithFormat:@"%.2f р.", product.price];
    cell.amountLabel.text = [NSString stringWithFormat:@"%i шт.", product.amount];
    return cell;
}

#pragma mark - Table view delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    return indexPath;
}

#pragma mark - Navigation

- (IBAction)unwindToListOfProducts:(UIStoryboardSegue*)sender
{
    if ([sender.sourceViewController conformsToProtocol:@protocol(AFProductPresenterProtocol)]) {
        AFStoreManager *manager = [AFStoreManager defaultInstance];
        [manager saveProduct:[sender.sourceViewController product] withCompletion:NULL];
        if (self.selectedIndexPath) {
            [self.tableView reloadRowsAtIndexPaths:@[self.selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    static NSString *AFSellerListAddProductSegueID = @"AFSellerListAddProductSegueID";
    AFProduct *product = nil;
    if ([segue.identifier isEqualToString:AFSellerListAddProductSegueID]) {
        // Если переход происходит по нажатию кнопки "плюс"
        self.selectedIndexPath = nil;
        product = [[AFProduct alloc] init];
    }else {
        product = self.products[self.selectedIndexPath.row];
    }
    if ([segue.destinationViewController conformsToProtocol:@protocol(AFProductPresenterProtocol)]) {
        id<AFProductPresenterProtocol> destVC = segue.destinationViewController;
        [destVC setProduct:product];
    }
}

#pragma mark - уведомления об изменении данных

- (void) showHeaderView
{
    if (!self.tableView.tableHeaderView) {
        UIView *someHeaderView = [[[NSBundle mainBundle] loadNibNamed:@"AFIndicatingTableViewHeader" owner:self options:nil] lastObject];
        self.tableView.tableHeaderView = someHeaderView;
    }
}

- (void) hideHeaderView
{
    self.tableView.tableHeaderView = nil;
    // не анимируется нормально, хоть тресни
  /*  [UIView animateWithDuration:2.0 animations:^{
        UIView *headerView = self.tableView.tableHeaderView;
        headerView.frame = CGRectMake(0, 0, CGRectGetWidth(headerView.frame), 0);
        self.tableView.tableHeaderView = headerView;
    }completion:^(BOOL finished) {
        self.tableView.tableHeaderView = nil;
    }]; */
}

- (void) storeManagerWillAddProduct:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [self showHeaderView];
        self.numberOfFutureProducts++;
    });
}

- (void) storeManagerDidAddProduct:(NSNotification *)notification
{
    dispatch_async(dispatch_get_main_queue(), ^{
        AFProduct *product = notification.userInfo[AFStoreItemInfoKey];
        if (product) {
            // добавляем новый продукт в массив - ставим его первым, все продукты отсортированы по createdAt, так что в дальнейшем он там и останется
            NSMutableArray *products = [NSMutableArray arrayWithCapacity:self.products.count + 1];
            [products addObject:product];
            [products addObjectsFromArray:self.products];
            self.products = products;
            
            [self.tableView beginUpdates];
            // убираем оповещение о новых продуктах
            self.numberOfFutureProducts--;
            if (self.numberOfFutureProducts == 0) {
                [self hideHeaderView];
            }
            // вставляем первую строку
            [self.tableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
            [self.tableView endUpdates];
        }
    });
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
