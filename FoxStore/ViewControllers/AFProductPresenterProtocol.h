//
//  AFProductPresenterProtocol.h
//  FoxStore
//
//  Created by Маша on 11.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFProduct.h"

@protocol AFProductPresenterProtocol <NSObject>
- (void) setProduct:(AFProduct *)product;
- (AFProduct *) product;
@end
