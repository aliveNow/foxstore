//
//  AFBuyerProductViewController.m
//  FoxStore
//
//  Created by Маша on 11.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import "AFBuyerProductViewController.h"
#import "AFStoreDataSource.h"

@interface AFBuyerProductViewController () <UIPickerViewDataSource, UIPickerViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *image;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UITextView *descriptionView;
@property (weak, nonatomic) IBOutlet UIButton *addToBasketButton;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@property (strong, nonatomic) AFBasket *basket;
@property (weak, nonatomic) AFBasketItem *basketItem;
@end

@implementation AFBuyerProductViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.basket = [AFStoreDataSource getCurrentBasket];
    self.basketItem = self.basket[self.product];
    
    self.image.image = [UIImage imageNamed:self.product.imagePath];
    self.titleLabel.text = self.product.title;
    self.priceLabel.text = [NSString stringWithFormat:@"%.2f р.", self.product.price];
    self.addToBasketButton.enabled = (self.product.amount > 0);
    if (self.basketItem) {
        [self.addToBasketButton setTitle:@"Сохранить" forState:UIControlStateNormal];
        [self.pickerView selectRow:self.basketItem.amount inComponent:0 animated:NO];
    }
}

#pragma mark - Kicked the basket

- (NSUInteger) selectedAmountOfProduct
{
    NSUInteger selectedRow = [self.pickerView selectedRowInComponent:0];
    return (self.basketItem) ? selectedRow : selectedRow + 1;
}

- (IBAction) addToBasketButtonWasPressed:(id)sender
{
    NSUInteger selectedAmount = [self selectedAmountOfProduct];
    if (selectedAmount > 0) {
        [self.basket addProduct:self.product withAmount:selectedAmount];
    }else {
        [self.basket removeProduct:self.product];
    }
    [self performSegueWithIdentifier:@"AFBuyerFromProductToListUnwindSegueID" sender:self];
#warning показать сообщение, что всё получилось
}

#pragma mark - picker view data source

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (self.basketItem) {
        return self.product.amount + 1; // добавляется 0 к возможным значениям, чтобы можно было удалить
    }
    return (self.product.amount > 0) ? self.product.amount : 1;
}

#pragma mark - picker view delegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSUInteger titleInteger = row;
    if (self.product.amount > 0 && !self.basketItem) {
        titleInteger++;
    }
    return [NSString stringWithFormat:@"%i", titleInteger];
}

@end
