//
//  AFBuyerListOfProductsVC.m
//  FoxStore
//
//  Created by Маша on 10.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import "AFBuyerListOfProductsVC.h"
#import "AFProductPresenterProtocol.h"
#import "AFBuyerProductCell.h"
#import "AFStoreDataSource.h"
#import "AFStoreManager.h"

#import <SVProgressHUD.h>

@interface AFBuyerListOfProductsVC ()
@property (strong, nonatomic) AFStoreDataSource *dataSource;
@property (strong, nonatomic) NSArray *products;
@property (strong, nonatomic) AFBasket *basket;

@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *basketButton;
@end

@implementation AFBuyerListOfProductsVC

- (void)viewDidLoad
{
    [super viewDidLoad];
#warning фабрику сделать?
    self.dataSource = [[AFStoreDataSource alloc] init];
    [self initializeData];
    [self reloadNavigationBar];
}

- (void) initializeData
{
    self.basket = [AFStoreDataSource getCurrentBasket];
    self.products = [self.dataSource getAllProducts];
}

- (void) reloadView
{
    [self.tableView reloadData];
    [self reloadNavigationBar];
}

- (void) reloadNavigationBar
{
    if (self.basket.countOfAllItems == 0) {
        self.basketButton.image = [UIImage imageNamed:@"shopping_cart_empty-32.png"];
        self.basketButton.enabled = NO;
    }else {
        self.basketButton.image =[UIImage imageNamed:@"shopping_cart_loaded-32.png"];
        self.basketButton.enabled = YES;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.products.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *productCellId = @"AFBuyerProductCellId";
    AFBuyerProductCell *cell = [tableView dequeueReusableCellWithIdentifier:productCellId forIndexPath:indexPath];
    AFProduct *product = self.products[indexPath.row];
#warning тестовая реализация, иначе [UIImage imageNamed] лучше не использовать
    cell.image.image = [UIImage imageNamed:product.imagePath];
    cell.titleLabel.text = product.title;
    cell.priceLabel.text = [NSString stringWithFormat:@"%.2f р.", product.price];
    cell.amountLabel.text = [NSString stringWithFormat:@"%i шт.", product.amount];
    AFBasketItem *basketItem = self.basket[product];
    if (basketItem.amount > 0) {
        cell.badgeString = [NSString stringWithFormat:@"%i", basketItem.amount];
        //cell.badgeColor = [UIColor colorWithRed:0.197 green:0.592 blue:0.219 alpha:1.000];
        cell.badge.radius = 5;
        cell.badge.fontSize = 15;
    }else {
        cell.badgeString = nil;
    }
    return cell;
}

#pragma mark - Table view delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    return indexPath;
}

#pragma mark - Navigation

- (IBAction)unwindToListOfProducts:(UIStoryboardSegue*)sender
{
    [self.tableView reloadRowsAtIndexPaths:@[self.selectedIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self reloadNavigationBar];
}

- (IBAction)unwindToListOfProductsFromBasket:(UIStoryboardSegue*)sender
{
    [self reloadView];
}

- (IBAction)unwindToListOfProductsFromBasketAfterOrder:(UIStoryboardSegue*)sender
{
    [SVProgressHUD show];
    [[AFStoreManager defaultInstance] sendOrder:self.basket withCompletion:^(BOOL success, NSError *error) {
        dispatch_async(dispatch_get_main_queue(), ^() {
            if (success) {
                [SVProgressHUD showSuccessWithStatus:@"Заказ отправлен!"];
                [self initializeData];
                [self reloadView];
            }else {
#warning поменять на alertView
                [SVProgressHUD showErrorWithStatus:@"Не получилось =("];
            }
        });
    }];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController conformsToProtocol:@protocol(AFProductPresenterProtocol)]) {
        id<AFProductPresenterProtocol> destVC = segue.destinationViewController;
        [destVC setProduct:self.products[self.selectedIndexPath.row]];
    }
}

@end
