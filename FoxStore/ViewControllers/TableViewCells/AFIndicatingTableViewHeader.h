//
//  AFIndicatingTableViewHeader.h
//  FoxStore
//
//  Created by Маша on 15.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AFIndicatingTableViewHeader : UIView
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@end
