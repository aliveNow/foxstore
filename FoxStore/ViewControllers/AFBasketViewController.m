//
//  AFBasketViewController.m
//  FoxStore
//
//  Created by Маша on 11.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import "AFBasketViewController.h"
#import "AFStoreDataSource.h"
#import "AFProductPresenterProtocol.h"

@interface AFBasketViewController ()
@property (strong, nonatomic) AFBasket *basket;
@property (strong, nonatomic) NSIndexPath *selectedIndexPath;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *orderButton;
@end

@implementation AFBasketViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.basket = [AFStoreDataSource getCurrentBasket];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(basketDidRemoveItem:) name:AFBasketDidRemoveItemNotification object:self.basket];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(basketDidChangeItem:) name:AFBasketDidChangeItemNotification object:self.basket];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.basket.countOfAllItems + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *basketProductCellId = @"BasketProductCellId";
    static NSString *basketTotalCellId = @"BasketTotalCellId";
    UITableViewCell *cell = nil;
    if (indexPath.row == self.basket.countOfAllItems) {
        cell = [tableView dequeueReusableCellWithIdentifier:basketTotalCellId forIndexPath:indexPath];
        cell.textLabel.text = @"Сумма: ";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%.2f р.", self.basket.totalPrice];
    }else {
        cell = [tableView dequeueReusableCellWithIdentifier:basketProductCellId forIndexPath:indexPath];
        AFBasketItem *item = self.basket[indexPath.row];
        cell.textLabel.text = item.product.title;
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%i шт.", item.amount];
    }
    return cell;
}

#pragma mark - Table view delegate

- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.selectedIndexPath = indexPath;
    return (indexPath.row != self.basket.countOfAllItems) ? indexPath : nil;
}

#pragma mark - Navigation

- (IBAction)unwindToListOfProducts:(UIStoryboardSegue*)sender
{

}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController conformsToProtocol:@protocol(AFProductPresenterProtocol)]) {
        id<AFProductPresenterProtocol> destVC = segue.destinationViewController;
        [destVC setProduct:[self.basket[self.selectedIndexPath.row] product]];
    }
}

#pragma mark - Removing Rows

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return (indexPath.row != self.basket.countOfAllItems);
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        [self.basket removeProductAtIndex:indexPath.row];
    }
}

#pragma mark - Basket Change

- (void) reloadNavigationBar
{
    self.orderButton.enabled = self.basket.countOfAllItems > 0;
}

- (void) reloadTotalPriceRow
{
    [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:self.basket.countOfAllItems inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self reloadNavigationBar];
}

- (void) basketDidRemoveItem:(NSNotification *)notification
{
    NSNumber *index = notification.userInfo[AFBasketItemIndexInfoKey];
    if (index) {
        [self.tableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index.integerValue inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        [self reloadTotalPriceRow];
    }
}

- (void) basketDidChangeItem:(NSNotification *)notification
{
    NSNumber *index = notification.userInfo[AFBasketItemIndexInfoKey];
    if (index) {
        [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:index.integerValue inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
        [self reloadTotalPriceRow];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
