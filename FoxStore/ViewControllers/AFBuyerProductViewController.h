//
//  AFBuyerProductViewController.h
//  FoxStore
//
//  Created by Маша on 11.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AFProductPresenterProtocol.h"

@interface AFBuyerProductViewController : UIViewController <AFProductPresenterProtocol>
@property (strong, nonatomic) AFProduct *product;
@end
