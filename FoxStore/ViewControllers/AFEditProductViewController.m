//
//  AFEditProductViewController.m
//  FoxStore
//
//  Created by Маша on 13.06.14.
//  Copyright (c) 2014 Alife Forest. All rights reserved.
//

#import "AFEditProductViewController.h"

@interface AFEditProductViewController ()
@property (weak, nonatomic) IBOutlet UITextField *titleField;
@property (weak, nonatomic) IBOutlet UITextField *priceField;
@property (weak, nonatomic) IBOutlet UITextField *amountField;
@property (weak, nonatomic) IBOutlet UITextView *descriptionTextView;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@end

@implementation AFEditProductViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.image.image = [UIImage imageNamed:self.product.imagePath];
    self.titleField.text = self.product.title;
    self.priceField.text = [NSString stringWithFormat:@"%.2f", self.product.price];
    self.amountField.text = [NSString stringWithFormat:@"%i", self.product.amount];
    //self.descriptionTextView.text = self.product.fullDescription;
}

- (IBAction)saveButtonWasPressed:(id)sender
{
    self.product.title = self.titleField.text;
    self.product.price = [self.priceField.text doubleValue];
    self.product.amount = [self.amountField.text integerValue];
    //self.product.fullDescription = self.descriptionTextView.text;
    [self performSegueWithIdentifier:@"AFUnwindToListOfProductsWithSave" sender:self];
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

@end
