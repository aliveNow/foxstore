
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// BlocksKit
#define COCOAPODS_POD_AVAILABLE_BlocksKit
#define COCOAPODS_VERSION_MAJOR_BlocksKit 2
#define COCOAPODS_VERSION_MINOR_BlocksKit 2
#define COCOAPODS_VERSION_PATCH_BlocksKit 3

// BlocksKit/All
#define COCOAPODS_POD_AVAILABLE_BlocksKit_All
#define COCOAPODS_VERSION_MAJOR_BlocksKit_All 2
#define COCOAPODS_VERSION_MINOR_BlocksKit_All 2
#define COCOAPODS_VERSION_PATCH_BlocksKit_All 3

// BlocksKit/Core
#define COCOAPODS_POD_AVAILABLE_BlocksKit_Core
#define COCOAPODS_VERSION_MAJOR_BlocksKit_Core 2
#define COCOAPODS_VERSION_MINOR_BlocksKit_Core 2
#define COCOAPODS_VERSION_PATCH_BlocksKit_Core 3

// BlocksKit/DynamicDelegate
#define COCOAPODS_POD_AVAILABLE_BlocksKit_DynamicDelegate
#define COCOAPODS_VERSION_MAJOR_BlocksKit_DynamicDelegate 2
#define COCOAPODS_VERSION_MINOR_BlocksKit_DynamicDelegate 2
#define COCOAPODS_VERSION_PATCH_BlocksKit_DynamicDelegate 3

// BlocksKit/MessageUI
#define COCOAPODS_POD_AVAILABLE_BlocksKit_MessageUI
#define COCOAPODS_VERSION_MAJOR_BlocksKit_MessageUI 2
#define COCOAPODS_VERSION_MINOR_BlocksKit_MessageUI 2
#define COCOAPODS_VERSION_PATCH_BlocksKit_MessageUI 3

// BlocksKit/UIKit
#define COCOAPODS_POD_AVAILABLE_BlocksKit_UIKit
#define COCOAPODS_VERSION_MAJOR_BlocksKit_UIKit 2
#define COCOAPODS_VERSION_MINOR_BlocksKit_UIKit 2
#define COCOAPODS_VERSION_PATCH_BlocksKit_UIKit 3

// M13BadgeView
#define COCOAPODS_POD_AVAILABLE_M13BadgeView
#define COCOAPODS_VERSION_MAJOR_M13BadgeView 1
#define COCOAPODS_VERSION_MINOR_M13BadgeView 0
#define COCOAPODS_VERSION_PATCH_M13BadgeView 2

// MagicalRecord
#define COCOAPODS_POD_AVAILABLE_MagicalRecord
#define COCOAPODS_VERSION_MAJOR_MagicalRecord 2
#define COCOAPODS_VERSION_MINOR_MagicalRecord 2
#define COCOAPODS_VERSION_PATCH_MagicalRecord 0

// MagicalRecord/Core
#define COCOAPODS_POD_AVAILABLE_MagicalRecord_Core
#define COCOAPODS_VERSION_MAJOR_MagicalRecord_Core 2
#define COCOAPODS_VERSION_MINOR_MagicalRecord_Core 2
#define COCOAPODS_VERSION_PATCH_MagicalRecord_Core 0

// RBStoryboardLink
#define COCOAPODS_POD_AVAILABLE_RBStoryboardLink
#define COCOAPODS_VERSION_MAJOR_RBStoryboardLink 0
#define COCOAPODS_VERSION_MINOR_RBStoryboardLink 1
#define COCOAPODS_VERSION_PATCH_RBStoryboardLink 0

// SVProgressHUD
#define COCOAPODS_POD_AVAILABLE_SVProgressHUD
#define COCOAPODS_VERSION_MAJOR_SVProgressHUD 1
#define COCOAPODS_VERSION_MINOR_SVProgressHUD 0
#define COCOAPODS_VERSION_PATCH_SVProgressHUD 0

// TDBadgedCell
#define COCOAPODS_POD_AVAILABLE_TDBadgedCell
// This library does not follow semantic-versioning,
// so we were not able to define version macros.
// Please contact the author.
// Version: 2.5.2.1.

// YHRoundBorderedButton
#define COCOAPODS_POD_AVAILABLE_YHRoundBorderedButton
#define COCOAPODS_VERSION_MAJOR_YHRoundBorderedButton 0
#define COCOAPODS_VERSION_MINOR_YHRoundBorderedButton 1
#define COCOAPODS_VERSION_PATCH_YHRoundBorderedButton 0

